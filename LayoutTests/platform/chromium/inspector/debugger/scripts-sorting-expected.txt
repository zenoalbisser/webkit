Tests scripts sorting in the scripts panel.

Debugger was enabled.

WebInspector.settings.showScriptFolders = true
    *Non*URL*path
— foo.com —
    block.js?block=foo
    ga.js
    lenta.ban?pg=4883&ifr=1
    lenta.ban?pg=5309&ifr=1
    top100.jcn?80674
/_js/production
    motor.js?1308927432
/foo/path
    bar.js?file=bar/zzz.js
    foo.js?file=bar/aaa.js
/i
    xgemius.js
/i/js
    jquery-1.5.1.min.js
    jquery.cookie.js

WebInspector.settings.showScriptFolders = false
*Non*URL*path
bar.js?file=bar/zzz.js
block.js?block=foo
foo.js?file=bar/aaa.js
ga.js
jquery-1.5.1.min.js
jquery.cookie.js
lenta.ban?pg=4883&ifr=1
lenta.ban?pg=5309&ifr=1
motor.js?1308927432
top100.jcn?80674
xgemius.js

WebInspector.settings.showScriptFolders = true
    *Non*URL*path
— foo.com —
    block.js?block=foo
    ga.js
    lenta.ban?pg=4883&ifr=1
    lenta.ban?pg=5309&ifr=1
    top100.jcn?80674
/_js/production
    motor.js?1308927432
/foo/path
    bar.js?file=bar/zzz.js
    foo.js?file=bar/aaa.js
/i
    xgemius.js
/i/js
    jquery-1.5.1.min.js
    jquery.cookie.js
Debugger was disabled.

