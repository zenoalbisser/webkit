layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x38
        RenderText {#text} at (0,0) size 212x19
          text run at (0,0) width 212: "This tests for a regression against "
        RenderInline {I} at (0,0) size 768x38
          RenderInline {A} at (0,0) size 348x19 [color=#0000EE]
            RenderText {#text} at (212,0) size 348x19
              text run at (212,0) width 348: "http://bugzilla.opendarwin.org/show_bug.cgi?id=6673"
          RenderText {#text} at (560,0) size 768x38
            text run at (560,0) width 4: " "
            text run at (564,0) width 204: "Selection highlight doesn't scroll"
            text run at (0,19) width 369: "along with an image contained in an overflow:scroll block"
        RenderText {#text} at (369,19) size 4x19
          text run at (369,19) width 4: "."
      RenderBlock {P} at (0,54) size 784x38
        RenderText {#text} at (0,0) size 748x38
          text run at (0,0) width 412: "There should be one contiguous highlight from \x{201C}elit\x{201D} to \x{201C}Etiam\x{201D}, "
          text run at (412,0) width 336: "including the orange square, and no other highlighted"
          text run at (0,19) width 36: "areas."
      RenderBlock {HR} at (0,108) size 784x2 [border: (1px inset #000000)]
layer at (8,126) size 100x200 clip at (8,126) size 84x200 scrollY 40 scrollHeight 304
  RenderBlock {DIV} at (0,118) size 100x200
    RenderText {#text} at (0,0) size 84x114
      text run at (0,0) width 84: "Lorem ipsum"
      text run at (0,19) width 51: "dolor sit"
      text run at (0,38) width 34: "amet,"
      text run at (0,57) width 78: "consectetuer"
      text run at (0,76) width 64: "adipiscing"
      text run at (0,95) width 27: "elit. "
    RenderImage {IMG} at (27,100) size 10x10
    RenderText {#text} at (37,95) size 83x209
      text run at (37,95) width 4: " "
      text run at (41,95) width 37: "Etiam"
      text run at (0,114) width 57: "et ipsum."
      text run at (0,133) width 31: "Nam"
      text run at (0,152) width 78: "consectetuer"
      text run at (0,171) width 81: "mi eget velit."
      text run at (0,190) width 83: "Sed nec risus"
      text run at (0,209) width 60: "vitae felis"
      text run at (0,228) width 39: "auctor"
      text run at (0,247) width 53: "ultricies."
      text run at (0,266) width 79: "Pellentesque"
      text run at (0,285) width 54: "aliquet..."
selection start: position 58 of child 0 {#text} of child 7 {DIV} of body
selection end:   position 11 of child 2 {#text} of child 7 {DIV} of body
