layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock {P} at (0,0) size 784x38
        RenderText {#text} at (0,0) size 762x38
          text run at (0,0) width 762: "This HTML tests if the 'while-space' property controls the behavors of line-break characters (U+000A and U+000D) in a"
          text run at (0,19) width 135: "complex text (Hindi)."
      RenderBlock {P} at (0,54) size 784x38
        RenderText {#text} at (0,0) size 783x38
          text run at (0,0) width 783: "If this test succeeds, you can see this Hindi word \"\x{935}\x{93F}\x{937}\x{92F}\x{94B}\x{902}\" repeated three times separated by a space in the first two"
          text run at (0,19) width 491: "paragraphs. In the third paragraphs, it'll be shown three times in separate lines."
      RenderBlock {P} at (0,108) size 784x19
        RenderText {#text} at (0,0) size 330x19
          text run at (0,0) width 330: "1. \x{935}\x{93F}\x{937}\x{92F}\x{94B}\x{902} \x{935}\x{93F}\x{937}\x{92F}\x{94B}\x{902}\x{D}\x{935}\x{93F}\x{937}\x{92F}\x{94B}\x{902}"
      RenderBlock {P} at (0,143) size 784x19
        RenderText {#text} at (0,0) size 330x19
          text run at (0,0) width 330: "2. \x{935}\x{93F}\x{937}\x{92F}\x{94B}\x{902} \x{935}\x{93F}\x{937}\x{92F}\x{94B}\x{902}\x{D}\x{935}\x{93F}\x{937}\x{92F}\x{94B}\x{902}"
      RenderBlock {P} at (0,178) size 784x57
        RenderText {#text} at (0,0) size 208x57
          text run at (0,0) width 12: "3."
          text run at (12,0) width 0: " "
          text run at (0,19) width 102: "\x{935}\x{93F}\x{937}\x{92F}\x{94B}\x{902}"
          text run at (102,19) width 0: " "
          text run at (0,38) width 208: "\x{935}\x{93F}\x{937}\x{92F}\x{94B}\x{902}\x{D}\x{935}\x{93F}\x{937}\x{92F}\x{94B}\x{902}"
