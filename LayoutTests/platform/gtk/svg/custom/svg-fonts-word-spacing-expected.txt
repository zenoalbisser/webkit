layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x574
      RenderBlock (anonymous) at (0,0) size 784x19
        RenderText {#text} at (0,0) size 72x19
          text run at (0,0) width 72: "no spacing:"
      RenderBlock {P} at (0,37) size 784x21
        RenderText {#text} at (0,0) size 85x21
          text run at (0,0) width 85: "abc abc abc"
      RenderBlock (anonymous) at (0,76) size 784x19
        RenderText {#text} at (0,0) size 294x19
          text run at (0,0) width 294: "word-spacing: 100px, all should look the same"
      RenderBlock {P} at (0,113) size 784x21
        RenderText {#text} at (0,0) size 285x21
          text run at (0,0) width 285: "abc abc abc"
      RenderBlock {P} at (0,152) size 784x21
        RenderText {#text} at (0,0) size 130x21
          text run at (0,0) width 130: "abc "
        RenderInline {SPAN} at (0,0) size 17x21
          RenderText {#text} at (130,0) size 17x21
            text run at (130,0) width 17: "ab"
        RenderText {#text} at (147,0) size 138x21
          text run at (147,0) width 138: "c abc"
      RenderBlock {P} at (0,191) size 784x21
        RenderText {#text} at (0,0) size 130x21
          text run at (0,0) width 130: "abc "
        RenderInline {SPAN} at (0,0) size 8x21
          RenderText {#text} at (130,0) size 8x21
            text run at (130,0) width 8: "a"
        RenderInline {SPAN} at (0,0) size 9x21
          RenderText {#text} at (138,0) size 9x21
            text run at (138,0) width 9: "b"
        RenderText {#text} at (147,0) size 138x21
          text run at (147,0) width 138: "c abc"
      RenderBlock {P} at (0,230) size 784x21
        RenderText {#text} at (0,0) size 130x21
          text run at (0,0) width 130: "abc "
        RenderInline {SPAN} at (0,0) size 8x21
          RenderText {#text} at (130,0) size 8x21
            text run at (130,0) width 8: "a"
        RenderText {#text} at (138,0) size 9x21
          text run at (138,0) width 9: "b"
        RenderInline {SPAN} at (0,0) size 8x21
          RenderText {#text} at (147,0) size 8x21
            text run at (147,0) width 8: "c"
        RenderText {#text} at (255,0) size 30x21
          text run at (255,0) width 30: " abc"
      RenderBlock {P} at (0,269) size 784x21
        RenderText {#text} at (0,0) size 138x21
          text run at (0,0) width 138: "abc a"
        RenderInline {SPAN} at (0,0) size 9x21
          RenderText {#text} at (138,0) size 9x21
            text run at (138,0) width 9: "b"
        RenderInline {SPAN} at (0,0) size 8x21
          RenderText {#text} at (147,0) size 8x21
            text run at (147,0) width 8: "c"
        RenderText {#text} at (255,0) size 30x21
          text run at (255,0) width 30: " abc"
      RenderBlock {P} at (0,308) size 784x21
        RenderText {#text} at (0,0) size 138x21
          text run at (0,0) width 138: "abc a"
        RenderInline {SPAN} at (0,0) size 9x21
          RenderText {#text} at (138,0) size 9x21
            text run at (138,0) width 9: "b"
        RenderText {#text} at (147,0) size 138x21
          text run at (147,0) width 138: "c abc"
      RenderBlock {P} at (0,347) size 784x21
        RenderText {#text} at (0,0) size 130x21
          text run at (0,0) width 130: "abc "
        RenderInline {SPAN} at (0,0) size 0x21
        RenderText {#text} at (130,0) size 130x21
          text run at (130,0) width 130: "abc "
        RenderInline {SPAN} at (0,0) size 0x21
        RenderText {#text} at (260,0) size 25x21
          text run at (260,0) width 25: "abc"
