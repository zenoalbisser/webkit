layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x484
  RenderBlock {HTML} at (0,0) size 800x484
    RenderBody {BODY} at (8,21) size 784x447
      RenderBlock {H1} at (0,0) size 784x42
        RenderText {#text} at (0,0) size 457x42
          text run at (0,0) width 457: "Minimum and Maximum Widths"
      RenderBlock {DIV} at (0,63) size 784x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 19x17
          RenderText {#text} at (37,6) size 19x17
            text run at (37,6) width 19: "div"
        RenderText {#text} at (56,3) size 440x22
          text run at (56,3) width 440: " should have a medium solid purple border, as should all the rest."
      RenderBlock {DIV} at (0,91) size 319x72 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 19x17
          RenderText {#text} at (37,6) size 19x17
            text run at (37,6) width 19: "div"
        RenderText {#text} at (56,3) size 310x44
          text run at (56,3) width 201: " should have a width of 40%. "
          text run at (257,3) width 56: "This is a"
          text run at (3,25) width 67: "reference "
        RenderInline {CODE} at (0,0) size 19x17
          RenderText {#text} at (70,28) size 19x17
            text run at (70,28) width 19: "div"
        RenderText {#text} at (89,25) size 188x22
          text run at (89,25) width 188: " and should work as long as"
        RenderInline {CODE} at (0,0) size 36x17
          RenderText {#text} at (3,50) size 36x17
            text run at (3,50) width 36: "width"
        RenderText {#text} at (39,47) size 50x22
          text run at (39,47) width 50: " works."
      RenderBlock {DIV} at (0,163) size 319x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 19x17
          RenderText {#text} at (37,6) size 19x17
            text run at (37,6) width 19: "div"
        RenderText {#text} at (56,3) size 197x22
          text run at (56,3) width 197: " should have a width of 40%."
      RenderBlock {DIV} at (0,191) size 319x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 19x17
          RenderText {#text} at (37,6) size 19x17
            text run at (37,6) width 19: "div"
        RenderText {#text} at (56,3) size 197x22
          text run at (56,3) width 197: " should have a width of 40%."
      RenderBlock {DIV} at (0,219) size 319x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 19x17
          RenderText {#text} at (37,6) size 19x17
            text run at (37,6) width 19: "div"
        RenderText {#text} at (56,3) size 197x22
          text run at (56,3) width 197: " should have a width of 40%."
      RenderBlock {DIV} at (0,247) size 319x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 19x17
          RenderText {#text} at (37,6) size 19x17
            text run at (37,6) width 19: "div"
        RenderText {#text} at (56,3) size 197x22
          text run at (56,3) width 197: " should have a width of 40%."
      RenderBlock {DIV} at (0,275) size 319x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 19x17
          RenderText {#text} at (37,6) size 19x17
            text run at (37,6) width 19: "div"
        RenderText {#text} at (56,3) size 197x22
          text run at (56,3) width 197: " should have a width of 40%."
      RenderBlock {DIV} at (0,303) size 319x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 19x17
          RenderText {#text} at (37,6) size 19x17
            text run at (37,6) width 19: "div"
        RenderText {#text} at (56,3) size 197x22
          text run at (56,3) width 197: " should have a width of 40%."
      RenderBlock {DIV} at (0,331) size 319x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 19x17
          RenderText {#text} at (37,6) size 19x17
            text run at (37,6) width 19: "div"
        RenderText {#text} at (56,3) size 197x22
          text run at (56,3) width 197: " should have a width of 40%."
      RenderBlock {DIV} at (0,359) size 319x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 19x17
          RenderText {#text} at (37,6) size 19x17
            text run at (37,6) width 19: "div"
        RenderText {#text} at (56,3) size 197x22
          text run at (56,3) width 197: " should have a width of 40%."
      RenderBlock {P} at (0,403) size 784x44
        RenderText {#text} at (0,0) size 217x22
          text run at (0,0) width 217: "If the browser does not support "
        RenderInline {CODE} at (0,0) size 66x17
          RenderText {#text} at (217,3) size 66x17
            text run at (217,3) width 66: "min-width"
        RenderText {#text} at (283,0) size 34x22
          text run at (283,0) width 34: " and "
        RenderInline {CODE} at (0,0) size 68x17
          RenderText {#text} at (317,3) size 68x17
            text run at (317,3) width 68: "max-width"
        RenderText {#text} at (385,0) size 185x22
          text run at (385,0) width 185: ", then the widths should be "
        RenderInline {CODE} at (0,0) size 29x17
          RenderText {#text} at (570,3) size 29x17
            text run at (570,3) width 29: "auto"
        RenderText {#text} at (599,0) size 8x22
          text run at (599,0) width 8: ", "
        RenderInline {CODE} at (0,0) size 28x17
          RenderText {#text} at (607,3) size 28x17
            text run at (607,3) width 28: "40%"
        RenderText {#text} at (635,0) size 8x22
          text run at (635,0) width 8: ", "
        RenderInline {CODE} at (0,0) size 28x17
          RenderText {#text} at (643,3) size 28x17
            text run at (643,3) width 28: "30%"
        RenderText {#text} at (671,0) size 8x22
          text run at (671,0) width 8: ", "
        RenderInline {CODE} at (0,0) size 28x17
          RenderText {#text} at (679,3) size 28x17
            text run at (679,3) width 28: "50%"
        RenderText {#text} at (707,0) size 8x22
          text run at (707,0) width 8: ", "
        RenderInline {CODE} at (0,0) size 28x17
          RenderText {#text} at (715,3) size 28x17
            text run at (715,3) width 28: "50%"
        RenderText {#text} at (743,0) size 8x22
          text run at (743,0) width 8: ", "
        RenderInline {CODE} at (0,0) size 28x17
          RenderText {#text} at (751,3) size 28x17
            text run at (751,3) width 28: "40%"
        RenderText {#text} at (779,0) size 4x22
          text run at (779,0) width 4: ","
        RenderInline {CODE} at (0,0) size 28x17
          RenderText {#text} at (0,25) size 28x17
            text run at (0,25) width 28: "30%"
        RenderText {#text} at (28,22) size 8x22
          text run at (28,22) width 8: ", "
        RenderInline {CODE} at (0,0) size 28x17
          RenderText {#text} at (36,25) size 28x17
            text run at (36,25) width 28: "40%"
        RenderText {#text} at (64,22) size 8x22
          text run at (64,22) width 8: ", "
        RenderInline {CODE} at (0,0) size 29x17
          RenderText {#text} at (72,25) size 29x17
            text run at (72,25) width 29: "auto"
        RenderText {#text} at (101,22) size 209x22
          text run at (101,22) width 209: " (with 70% margin-right), and "
        RenderInline {CODE} at (0,0) size 29x17
          RenderText {#text} at (310,25) size 29x17
            text run at (310,25) width 29: "auto"
        RenderText {#text} at (339,22) size 4x22
          text run at (339,22) width 4: "."
