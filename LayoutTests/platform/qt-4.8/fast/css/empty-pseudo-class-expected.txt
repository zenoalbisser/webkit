layer at (0,0) size 784x1784
  RenderView at (0,0) size 784x600
layer at (0,0) size 784x1784
  RenderBlock {HTML} at (0,0) size 784x1784
    RenderBody {BODY} at (8,16) size 768x1720 [bgcolor=#FFFFFF]
      RenderBlock {P} at (0,0) size 768x21
        RenderText {#text} at (0,0) size 184x21
          text run at (0,0) width 184: "This page is part of the "
        RenderInline {A} at (0,0) size 79x21 [color=#0000EE]
          RenderText {#text} at (184,0) size 79x21
            text run at (184,0) width 79: "CSS3.info"
        RenderText {#text} at (263,0) size 5x21
          text run at (263,0) width 5: " "
        RenderInline {A} at (0,0) size 139x21 [color=#0000EE]
          RenderText {#text} at (268,0) size 139x21
            text run at (268,0) width 139: "CSS selectors test"
        RenderText {#text} at (407,0) size 146x21
          text run at (407,0) width 146: ". See more info on "
        RenderInline {A} at (0,0) size 116x21 [color=#0000EE]
          RenderText {#text} at (553,0) size 116x21
            text run at (553,0) width 116: "CSS3 selectors"
        RenderText {#text} at (669,0) size 5x21
          text run at (669,0) width 5: "."
      RenderBlock {DIV} at (0,37) size 768x0 [bgcolor=#009900]
      RenderBlock {OL} at (0,37) size 768x1683
        RenderListItem {LI} at (40,0) size 614x206 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 582x24 [bgcolor=#990000]
            RenderBlock {DIV} at (0,0) size 582x24 [bgcolor=#009900]
          RenderBlock {PRE} at (16,53) size 582x84 [bgcolor=#FFFFFF]
            RenderListMarker at (-41,6) size 20x21: "1"
            RenderText {#text} at (6,9) size 78x69
              text run at (6,9) width 51: ":empty {"
              text run at (57,9) width 0: " "
              text run at (6,27) width 4: "}"
              text run at (10,27) width 0: " "
              text run at (6,44) width 0: " "
              text run at (6,61) width 78: "<div></div>"
          RenderBlock {P} at (16,153) size 582x21
            RenderText {#text} at (0,0) size 528x21
              text run at (0,0) width 528: "The CSS selector should match the div element, because it is empty"
        RenderListItem {LI} at (40,254) size 614x206 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 582x24 [bgcolor=#990000]
            RenderBlock {DIV} at (0,0) size 582x24 [bgcolor=#009900]
          RenderBlock {PRE} at (16,53) size 582x84 [bgcolor=#FFFFFF]
            RenderListMarker at (-41,6) size 20x21: "2"
            RenderText {#text} at (6,9) size 230x69
              text run at (6,9) width 51: ":empty {"
              text run at (57,9) width 0: " "
              text run at (6,27) width 4: "}"
              text run at (10,27) width 0: " "
              text run at (6,44) width 0: " "
              text run at (6,61) width 230: "<div><!-- Just a comment --></div>"
          RenderBlock {P} at (16,153) size 582x21
            RenderText {#text} at (0,0) size 528x21
              text run at (0,0) width 528: "The CSS selector should match the div element, because it is empty"
        RenderListItem {LI} at (40,508) size 614x227 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 582x24 [bgcolor=#009900]
            RenderBlock {DIV} at (0,0) size 582x24
          RenderBlock {PRE} at (16,53) size 582x84 [bgcolor=#FFFFFF]
            RenderListMarker at (-41,6) size 20x21: "3"
            RenderText {#text} at (6,9) size 82x69
              text run at (6,9) width 51: ":empty {"
              text run at (57,9) width 0: " "
              text run at (6,27) width 4: "}"
              text run at (10,27) width 0: " "
              text run at (6,44) width 0: " "
              text run at (6,61) width 82: "<div> </div>"
          RenderBlock {P} at (16,153) size 582x42
            RenderText {#text} at (0,0) size 537x42
              text run at (0,0) width 537: "The CSS selector should not match the div element, because it is not"
              text run at (0,21) width 48: "empty"
        RenderListItem {LI} at (40,783) size 614x278 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 582x45 [bgcolor=#009900]
            RenderBlock {DIV} at (0,0) size 582x45
              RenderListMarker at (-41,12) size 20x21: "4"
              RenderText {#text} at (12,12) size 5x21
                text run at (12,12) width 5: "."
          RenderBlock {PRE} at (16,74) size 582x114 [bgcolor=#FFFFFF]
            RenderText {#text} at (6,6) size 171x102
              text run at (6,6) width 51: ":empty {"
              text run at (57,6) width 0: " "
              text run at (6,23) width 4: "}"
              text run at (10,23) width 0: " "
              text run at (6,40) width 0: " "
              text run at (6,57) width 39: "<div> "
              text run at (45,57) width 0: " "
              text run at (6,74) width 171: "   How about regular text..."
              text run at (177,74) width 0: " "
              text run at (6,91) width 43: "</div>"
          RenderBlock {P} at (16,204) size 582x42
            RenderText {#text} at (0,0) size 537x42
              text run at (0,0) width 537: "The CSS selector should not match the div element, because it is not"
              text run at (0,21) width 48: "empty"
        RenderListItem {LI} at (40,1109) size 614x227 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 582x24 [bgcolor=#009900]
            RenderBlock {DIV} at (0,0) size 582x24
              RenderBlock {BLOCKQUOTE} at (12,12) size 558x0 [bgcolor=#990000]
          RenderBlock {PRE} at (16,53) size 582x84 [bgcolor=#FFFFFF]
            RenderListMarker at (-41,6) size 20x21: "5"
            RenderText {#text} at (6,9) size 258x69
              text run at (6,9) width 51: ":empty {"
              text run at (57,9) width 0: " "
              text run at (6,27) width 4: "}"
              text run at (10,27) width 0: " "
              text run at (6,44) width 0: " "
              text run at (6,61) width 258: "<div><blockquote></blockquote></div>"
          RenderBlock {P} at (16,153) size 582x42
            RenderText {#text} at (0,0) size 537x42
              text run at (0,0) width 537: "The CSS selector should not match the div element, because it is not"
              text run at (0,21) width 48: "empty"
        RenderListItem {LI} at (40,1384) size 614x299 [bgcolor=#AAAAAA]
          RenderBlock {DIV} at (16,16) size 582x24 [bgcolor=#009900]
            RenderBlock {DIV} at (0,0) size 582x24
              RenderBlock {DIV} at (12,12) size 558x0 [bgcolor=#990000]
          RenderBlock {PRE} at (16,53) size 582x135 [bgcolor=#FFFFFF]
            RenderListMarker at (-41,6) size 20x21: "6"
            RenderText {#text} at (6,9) size 323x120
              text run at (6,9) width 51: ":empty {"
              text run at (57,9) width 0: " "
              text run at (6,27) width 4: "}"
              text run at (10,27) width 0: " "
              text run at (6,44) width 0: " "
              text run at (6,61) width 191: "<div id='appendChild'></div>"
              text run at (197,61) width 0: " "
              text run at (6,78) width 0: " "
              text run at (6,95) width 323: "var ib = document.getElementById('appendChild');"
              text run at (329,95) width 0: " "
              text run at (6,112) width 309: "ib.appendChild(document.createElement(\"div\"));"
          RenderBlock {P} at (16,204) size 582x63
            RenderText {#text} at (0,0) size 580x63
              text run at (0,0) width 580: "The CSS selector should not match the orignal div element, because it will"
              text run at (0,21) width 107: "not be empty "
              text run at (107,21) width 458: "anymore after the Javascript code appends a child element"
              text run at (0,42) width 36: "to it."
