layer at (0,0) size 480x360
  RenderView at (0,0) size 480x360
layer at (0,0) size 480x360
  RenderSVGRoot {svg} at (0,0) size 480x360
    RenderSVGHiddenContainer {defs} at (0,0) size 0x0
    RenderSVGContainer {g} at (0,0) size 480x360
      RenderSVGContainer {g} at (0,0) size 480x360
        RenderSVGPath {rect} at (0,0) size 480x360 [fill={[type=SOLID] [color=#FF0000]}] [x=0.00] [y=0.00] [width=2000.00] [height=2000.00]
        RenderSVGPath {rect} at (0,0) size 480x360 [fill={[type=SOLID] [color=#FFFFFF]}] [x=0.00] [y=0.00] [width=480.00] [height=360.00]
      RenderSVGText {text} at (125,8) size 205x16 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 205x16
          chunk 1 text run 1 at (125.00,20.00) startOffset 0 endOffset 35 width 205.00: "Initial viewport and CSS units test"
      RenderSVGContainer {g} at (20,65) size 405x199 [transform={m=((1.00,0.00)(0.00,1.00)) t=(0.00,60.00)}]
        RenderSVGText {text} at (20,5) size 24x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 24x17
            chunk 1 text run 1 at (20.00,18.00) startOffset 0 endOffset 3 width 24.00: "200"
        RenderSVGText {text} at (230,7) size 195x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 195x17
            chunk 1 text run 1 at (230.00,20.00) startOffset 0 endOffset 31 width 195.00: "User space units (no specifier)"
        RenderSVGPath {rect} at (20,80) size 200x1 [fill={[type=SOLID] [color=#000000]}] [x=20.00] [y=20.00] [width=200.00] [height=1.00]
        RenderSVGText {text} at (20,25) size 43x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 43x17
            chunk 1 text run 1 at (20.00,38.00) startOffset 0 endOffset 6 width 43.00: "200 px"
        RenderSVGText {text} at (230,27) size 64x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 64x17
            chunk 1 text run 1 at (230.00,40.00) startOffset 0 endOffset 11 width 64.00: "Pixels (px)"
        RenderSVGPath {rect} at (20,100) size 200x1 [fill={[type=SOLID] [color=#000000]}] [x=20.00] [y=40.00] [width=200.00] [height=1.00]
        RenderSVGText {text} at (20,45) size 203x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 203x17
            chunk 1 text run 1 at (20.00,58.00) startOffset 0 endOffset 31 width 203.00: "20 em = 200 px (font-size=10px)"
        RenderSVGText {text} at (230,47) size 155x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 155x17
            chunk 1 text run 1 at (230.00,60.00) startOffset 0 endOffset 26 width 155.00: "Relative to font size (em)"
        RenderSVGContainer {g} at (20,120) size 200x1
          RenderSVGPath {rect} at (20,120) size 200x1 [fill={[type=SOLID] [color=#000000]}] [x=20.00] [y=60.00] [width=200.00] [height=1.00]
        RenderSVGText {text} at (20,65) size 34x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 34x17
            chunk 1 text run 1 at (20.00,78.00) startOffset 0 endOffset 5 width 34.00: "40 ex"
        RenderSVGText {text} at (230,67) size 179x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 179x17
            chunk 1 text run 1 at (230.00,80.00) startOffset 0 endOffset 30 width 179.00: "Relative to font x-height (ex)"
        RenderSVGContainer {g} at (20,140) size 200x1
          RenderSVGPath {rect} at (20,140) size 200x1 [fill={[type=SOLID] [color=#000000]}] [x=20.00] [y=80.00] [width=200.00] [height=1.00]
        RenderSVGText {text} at (20,85) size 107x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 107x17
            chunk 1 text run 1 at (20.00,98.00) startOffset 0 endOffset 15 width 107.00: "41.67% = 200 px"
        RenderSVGText {text} at (230,87) size 95x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 95x17
            chunk 1 text run 1 at (230.00,100.00) startOffset 0 endOffset 14 width 95.00: "Percentage (%)"
        RenderSVGPath {rect} at (20,160) size 201x1 [fill={[type=SOLID] [color=#000000]}] [x=20.00] [y=100.00] [width=200.02] [height=1.00]
        RenderSVGText {text} at (20,105) size 25x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 25x17
            chunk 1 text run 1 at (20.00,118.00) startOffset 0 endOffset 4 width 25.00: "1 in"
        RenderSVGText {text} at (230,107) size 68x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 68x17
            chunk 1 text run 1 at (230.00,120.00) startOffset 0 endOffset 11 width 68.00: "Inches (in)"
        RenderSVGPath {rect} at (20,180) size 96x1 [fill={[type=SOLID] [color=#000000]}] [x=20.00] [y=120.00] [width=96.00] [height=1.00]
        RenderSVGText {text} at (20,125) size 92x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 92x17
            chunk 1 text run 1 at (20.00,138.00) startOffset 0 endOffset 14 width 92.00: "2.54 cm = 1 in"
        RenderSVGText {text} at (230,127) size 110x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 110x17
            chunk 1 text run 1 at (230.00,140.00) startOffset 0 endOffset 16 width 110.00: "Centimeters (cm)"
        RenderSVGPath {rect} at (20,200) size 96x1 [fill={[type=SOLID] [color=#000000]}] [x=20.00] [y=140.00] [width=96.00] [height=1.00]
        RenderSVGText {text} at (20,145) size 97x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 97x17
            chunk 1 text run 1 at (20.00,158.00) startOffset 0 endOffset 14 width 97.00: "25.4 mm = 1 in"
        RenderSVGText {text} at (230,147) size 108x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 108x17
            chunk 1 text run 1 at (230.00,160.00) startOffset 0 endOffset 16 width 108.00: "Millimeters (mm)"
        RenderSVGPath {rect} at (20,220) size 96x1 [fill={[type=SOLID] [color=#000000]}] [x=20.00] [y=160.00] [width=96.00] [height=1.00]
        RenderSVGText {text} at (20,165) size 70x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 70x17
            chunk 1 text run 1 at (20.00,178.00) startOffset 0 endOffset 11 width 70.00: "72pt = 1 in"
        RenderSVGText {text} at (230,167) size 65x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 65x17
            chunk 1 text run 1 at (230.00,180.00) startOffset 0 endOffset 11 width 65.00: "Points (pt)"
        RenderSVGPath {rect} at (20,240) size 96x1 [fill={[type=SOLID] [color=#000000]}] [x=20.00] [y=180.00] [width=96.00] [height=1.00]
        RenderSVGText {text} at (20,185) size 64x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 64x17
            chunk 1 text run 1 at (20.00,198.00) startOffset 0 endOffset 10 width 64.00: "6pc = 1 in"
        RenderSVGText {text} at (230,187) size 61x17 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 61x17
            chunk 1 text run 1 at (230.00,200.00) startOffset 0 endOffset 10 width 61.00: "Picas (pc)"
        RenderSVGPath {rect} at (20,260) size 96x1 [fill={[type=SOLID] [color=#000000]}] [x=20.00] [y=200.00] [width=96.00] [height=1.00]
    RenderSVGContainer {g} at (10,309) size 253x40
      RenderSVGText {text} at (10,309) size 253x40 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 253x40
          chunk 1 text run 1 at (10.00,340.00) startOffset 0 endOffset 16 width 253.00: "$Revision: 1.7 $"
    RenderSVGPath {rect} at (0,0) size 480x360 [stroke={[type=SOLID] [color=#000000]}] [x=1.00] [y=1.00] [width=478.00] [height=358.00]
