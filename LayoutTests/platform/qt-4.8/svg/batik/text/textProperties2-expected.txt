layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 450x500
  RenderSVGRoot {svg} at (12,25) size 438x308
    RenderSVGHiddenContainer {defs} at (0,0) size 0x0
    RenderSVGContainer {g} at (12,25) size 438x308
      RenderSVGText {text} at (127,25) size 196x20 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 196x20
          chunk 1 (middle anchor) text run 1 at (127.00,40.00) startOffset 0 endOffset 28 width 196.00: "Text display and visibility."
      RenderSVGText {text} at (138,58) size 174x22 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 174x22
          chunk 1 (middle anchor) text run 1 at (138.00,74.00) startOffset 0 endOffset 27 width 174.00: "Visibility on text & tspans"
      RenderSVGText {text} at (142,184) size 166x22 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 166x22
          chunk 1 (middle anchor) text run 1 at (142.50,200.00) startOffset 0 endOffset 24 width 165.00: "Display on text & tspans"
      RenderSVGContainer {g} at (12,81) size 426x252
        RenderSVGPath {rect} at (12,81) size 426x27 [stroke={[type=SOLID] [color=#000000]}] [fill={[type=SOLID] [color=#EEEEEE]}] [x=12.50] [y=82.00] [width=425.00] [height=25.00]
        RenderSVGPath {rect} at (12,106) size 426x27 [stroke={[type=SOLID] [color=#000000]}] [fill={[type=SOLID] [color=#CCCCCC]}] [x=12.50] [y=107.00] [width=425.00] [height=25.00]
        RenderSVGPath {rect} at (12,131) size 426x27 [stroke={[type=SOLID] [color=#000000]}] [fill={[type=SOLID] [color=#EEEEEE]}] [x=12.50] [y=132.00] [width=425.00] [height=25.00]
        RenderSVGPath {rect} at (12,156) size 426x27 [stroke={[type=SOLID] [color=#000000]}] [fill={[type=SOLID] [color=#CCCCCC]}] [x=12.50] [y=157.00] [width=425.00] [height=25.00]
        RenderSVGPath {line} at (339,82) size 2x100 [stroke={[type=SOLID] [color=#000000]}] [fill={[type=SOLID] [color=#000000]}] [x1=340.00] [y1=82.00] [x2=340.00] [y2=182.00]
        RenderSVGPath {rect} at (12,206) size 426x27 [stroke={[type=SOLID] [color=#000000]}] [fill={[type=SOLID] [color=#EEEEEE]}] [x=12.50] [y=207.00] [width=425.00] [height=25.00]
        RenderSVGPath {rect} at (12,231) size 426x27 [stroke={[type=SOLID] [color=#000000]}] [fill={[type=SOLID] [color=#CCCCCC]}] [x=12.50] [y=232.00] [width=425.00] [height=25.00]
        RenderSVGPath {rect} at (12,256) size 426x27 [stroke={[type=SOLID] [color=#000000]}] [fill={[type=SOLID] [color=#EEEEEE]}] [x=12.50] [y=257.00] [width=425.00] [height=25.00]
        RenderSVGPath {rect} at (12,281) size 426x27 [stroke={[type=SOLID] [color=#000000]}] [fill={[type=SOLID] [color=#CCCCCC]}] [x=12.50] [y=282.00] [width=425.00] [height=25.00]
        RenderSVGPath {rect} at (12,306) size 426x27 [stroke={[type=SOLID] [color=#000000]}] [fill={[type=SOLID] [color=#EEEEEE]}] [x=12.50] [y=307.00] [width=425.00] [height=25.00]
        RenderSVGPath {line} at (339,207) size 2x125 [stroke={[type=SOLID] [color=#000000]}] [fill={[type=SOLID] [color=#000000]}] [x1=340.00] [y1=207.00] [x2=340.00] [y2=332.00]
      RenderSVGText {text} at (360,81) size 83x21 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 83x21
          chunk 1 text run 1 at (360.00,97.00) startOffset 0 endOffset 13 width 83.00: "vis, vis, vis"
      RenderSVGText {text} at (360,106) size 88x21 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 88x21
          chunk 1 text run 1 at (360.00,122.00) startOffset 0 endOffset 13 width 88.00: "vis, hid, vis"
      RenderSVGText {text} at (360,131) size 93x21 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 93x21
          chunk 1 text run 1 at (360.00,147.00) startOffset 0 endOffset 13 width 93.00: "vis, hid, hid"
      RenderSVGText {text} at (360,156) size 93x21 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 93x21
          chunk 1 text run 1 at (360.00,172.00) startOffset 0 endOffset 13 width 93.00: "hid, vis, hid"
      RenderSVGText {text} at (360,207) size 155x21 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 155x21
          chunk 1 text run 1 at (360.00,223.00) startOffset 0 endOffset 22 width 155.00: "inline, inline, inline"
      RenderSVGText {text} at (360,232) size 149x21 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 149x21
          chunk 1 text run 1 at (360.00,248.00) startOffset 0 endOffset 20 width 149.00: "inline, inline, none"
      RenderSVGText {text} at (360,257) size 149x21 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 149x21
          chunk 1 text run 1 at (360.00,273.00) startOffset 0 endOffset 20 width 149.00: "inline, none, inline"
      RenderSVGText {text} at (360,282) size 143x21 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 143x21
          chunk 1 text run 1 at (360.00,298.00) startOffset 0 endOffset 18 width 143.00: "inline, none, none"
      RenderSVGText {text} at (360,307) size 143x21 contains 1 chunk(s)
        RenderSVGInlineText {#text} at (0,0) size 143x21
          chunk 1 text run 1 at (360.00,323.00) startOffset 0 endOffset 18 width 143.00: "none, inline, none"
      RenderSVGContainer {g} at (25,80) size 377x228 [transform={m=((1.25,0.00)(0.00,1.25)) t=(0.00,12.50)}]
        RenderSVGText {text} at (20,54) size 301x22 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 36x22
            chunk 1 text run 1 at (20.00,70.00) startOffset 0 endOffset 5 width 35.20: "Text "
          RenderSVGTSpan {tspan} at (0,0) size 192x22
            RenderSVGInlineText {#text} at (35,0) size 54x22
              chunk 1 text run 1 at (55.20,70.00) startOffset 0 endOffset 8 width 53.60: "tspan 1 "
            RenderSVGTSpan {tspan} at (0,0) size 51x22
              RenderSVGInlineText {#text} at (88,0) size 51x22
                chunk 1 text run 1 at (108.80,70.00) startOffset 0 endOffset 7 width 49.60: "tspan 2"
            RenderSVGInlineText {#text} at (138,0) size 89x22
              chunk 1 text run 1 at (158.40,70.00) startOffset 0 endOffset 12 width 88.00: " more span 1"
          RenderSVGInlineText {#text} at (226,0) size 75x22
            chunk 1 text run 1 at (246.40,70.00) startOffset 0 endOffset 12 width 73.60: " finish text"
        RenderSVGText {text} at (20,74) size 301x22 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 36x22
            chunk 1 text run 1 at (20.00,90.00) startOffset 0 endOffset 5 width 35.20: "Text "
          RenderSVGTSpan {tspan} at (0,0) size 192x22
            RenderSVGInlineText {#text} at (35,0) size 54x22
              chunk 1 text run 1 at (55.20,90.00) startOffset 0 endOffset 8 width 53.60: "tspan 1 "
            RenderSVGTSpan {tspan} at (0,0) size 51x22
              RenderSVGInlineText {#text} at (88,0) size 51x22
                chunk 1 text run 1 at (108.80,90.00) startOffset 0 endOffset 7 width 49.60: "tspan 2"
            RenderSVGInlineText {#text} at (138,0) size 89x22
              chunk 1 text run 1 at (158.40,90.00) startOffset 0 endOffset 12 width 88.00: " more span 1"
          RenderSVGInlineText {#text} at (226,0) size 75x22
            chunk 1 text run 1 at (246.40,90.00) startOffset 0 endOffset 12 width 73.60: " finish text"
        RenderSVGText {text} at (20,94) size 301x22 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 36x22
            chunk 1 text run 1 at (20.00,110.00) startOffset 0 endOffset 5 width 35.20: "Text "
          RenderSVGTSpan {tspan} at (0,0) size 192x22
            RenderSVGInlineText {#text} at (35,0) size 54x22
              chunk 1 text run 1 at (55.20,110.00) startOffset 0 endOffset 8 width 53.60: "tspan 1 "
            RenderSVGTSpan {tspan} at (0,0) size 51x22
              RenderSVGInlineText {#text} at (88,0) size 51x22
                chunk 1 text run 1 at (108.80,110.00) startOffset 0 endOffset 7 width 49.60: "tspan 2"
            RenderSVGInlineText {#text} at (138,0) size 89x22
              chunk 1 text run 1 at (158.40,110.00) startOffset 0 endOffset 12 width 88.00: " more span 1"
          RenderSVGInlineText {#text} at (226,0) size 75x22
            chunk 1 text run 1 at (246.40,110.00) startOffset 0 endOffset 12 width 73.60: " finish text"
        RenderSVGText {text} at (20,114) size 301x22 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 36x22
            chunk 1 text run 1 at (20.00,130.00) startOffset 0 endOffset 5 width 35.20: "Text "
          RenderSVGTSpan {tspan} at (0,0) size 192x22
            RenderSVGInlineText {#text} at (35,0) size 54x22
              chunk 1 text run 1 at (55.20,130.00) startOffset 0 endOffset 8 width 53.60: "tspan 1 "
            RenderSVGTSpan {tspan} at (0,0) size 51x22
              RenderSVGInlineText {#text} at (88,0) size 51x22
                chunk 1 text run 1 at (108.80,130.00) startOffset 0 endOffset 7 width 49.60: "tspan 2"
            RenderSVGInlineText {#text} at (138,0) size 89x22
              chunk 1 text run 1 at (158.40,130.00) startOffset 0 endOffset 12 width 88.00: " more span 1"
          RenderSVGInlineText {#text} at (226,0) size 75x22
            chunk 1 text run 1 at (246.40,130.00) startOffset 0 endOffset 12 width 73.60: " finish text"
        RenderSVGText {text} at (20,154) size 301x22 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 36x22
            chunk 1 text run 1 at (20.00,170.00) startOffset 0 endOffset 5 width 35.20: "Text "
          RenderSVGTSpan {tspan} at (0,0) size 192x22
            RenderSVGInlineText {#text} at (35,0) size 54x22
              chunk 1 text run 1 at (55.20,170.00) startOffset 0 endOffset 8 width 53.60: "tspan 1 "
            RenderSVGTSpan {tspan} at (0,0) size 55x22
              RenderSVGInlineText {#text} at (88,0) size 55x22
                chunk 1 text run 1 at (108.80,170.00) startOffset 0 endOffset 8 width 53.60: "tspan 2 "
            RenderSVGInlineText {#text} at (142,0) size 85x22
              chunk 1 text run 1 at (162.40,170.00) startOffset 0 endOffset 11 width 84.00: "more span 1"
          RenderSVGInlineText {#text} at (226,0) size 75x22
            chunk 1 text run 1 at (246.40,170.00) startOffset 0 endOffset 12 width 73.60: " finish text"
        RenderSVGText {text} at (20,174) size 247x22 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 36x22
            chunk 1 text run 1 at (20.00,190.00) startOffset 0 endOffset 5 width 35.20: "Text "
          RenderSVGTSpan {tspan} at (0,0) size 138x22
            RenderSVGInlineText {#text} at (35,0) size 54x22
              chunk 1 text run 1 at (55.20,190.00) startOffset 0 endOffset 8 width 53.60: "tspan 1 "
            RenderSVGInlineText {#text} at (88,0) size 85x22
              chunk 1 text run 1 at (108.80,190.00) startOffset 0 endOffset 11 width 84.00: "more span 1"
          RenderSVGInlineText {#text} at (172,0) size 75x22
            chunk 1 text run 1 at (192.80,190.00) startOffset 0 endOffset 12 width 73.60: " finish text"
        RenderSVGText {text} at (20,194) size 105x22 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 36x22
            chunk 1 text run 1 at (20.00,210.00) startOffset 0 endOffset 5 width 35.20: "Text "
          RenderSVGInlineText {#text} at (35,0) size 70x22
            chunk 1 text run 1 at (55.20,210.00) startOffset 0 endOffset 11 width 69.60: "finish text"
        RenderSVGText {text} at (20,214) size 105x22 contains 1 chunk(s)
          RenderSVGInlineText {#text} at (0,0) size 36x22
            chunk 1 text run 1 at (20.00,230.00) startOffset 0 endOffset 5 width 35.20: "Text "
          RenderSVGInlineText {#text} at (35,0) size 70x22
            chunk 1 text run 1 at (55.20,230.00) startOffset 0 endOffset 11 width 69.60: "finish text"
    RenderSVGContainer {use} at (0,0) size 0x0
      RenderSVGContainer {g} at (0,0) size 0x0
