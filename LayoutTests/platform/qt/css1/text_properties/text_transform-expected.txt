layer at (0,0) size 784x882
  RenderView at (0,0) size 784x600
layer at (0,0) size 784x882
  RenderBlock {HTML} at (0,0) size 784x882
    RenderBody {BODY} at (8,8) size 768x866 [bgcolor=#CCCCCC]
      RenderBlock {P} at (0,0) size 768x22
        RenderText {#text} at (0,0) size 380x22
          text run at (0,0) width 380: "The style declarations which apply to the text below are:"
      RenderBlock {PRE} at (0,38) size 768x85
        RenderText {#text} at (0,0) size 190x85
          text run at (0,0) width 152: ".ttn {text-transform: none;}"
          text run at (152,0) width 0: " "
          text run at (0,17) width 185: ".cap {text-transform: capitalize;}"
          text run at (185,17) width 0: " "
          text run at (0,34) width 190: ".upp {text-transform: uppercase;}"
          text run at (190,34) width 0: " "
          text run at (0,51) width 186: ".low {text-transform: lowercase;}"
          text run at (186,51) width 0: " "
          text run at (0,68) width 0: " "
      RenderBlock {HR} at (0,136) size 768x2 [border: (1px inset #000000)]
      RenderBlock {P} at (0,154) size 768x44
        RenderText {#text} at (0,0) size 734x44
          text run at (0,0) width 734: "This page tests the 'text-transform' property of CSS1. This paragraph has no text transformation and should"
          text run at (0,22) width 105: "appear normal."
      RenderBlock {P} at (0,214) size 768x66
        RenderText {#text} at (0,0) size 760x66
          text run at (0,0) width 733: "This Paragraph Is Capitalized And The First Letter In Each Word Should Therefore Appear In Uppercase."
          text run at (0,22) width 760: "Words That Are In Uppercase In The Source (E.G. USA) Should Remain So. There Should Be A Capital Letter"
          text run at (0,44) width 668: "After A Non-Breaking Space (&Nbsp;). Both Those Characters Appear In The Previous Sentence."
      RenderBlock {P} at (0,296) size 768x44
        RenderText {#text} at (0,0) size 762x44
          text run at (0,0) width 762: "Words with inline elements inside them should only capitalize the first letter of the word. Therefore, the last word"
          text run at (0,22) width 367: "in this sentence should have one, and only one, capital "
        RenderInline {SPAN} at (0,0) size 42x22
          RenderText {#text} at (367,22) size 18x22
            text run at (367,22) width 18: "Le"
          RenderInline {SPAN} at (0,0) size 10x22
            RenderText {#text} at (385,22) size 10x22
              text run at (385,22) width 10: "tt"
          RenderText {#text} at (395,22) size 14x22
            text run at (395,22) width 14: "er"
        RenderText {#text} at (409,22) size 4x22
          text run at (409,22) width 4: "."
      RenderBlock {P} at (0,356) size 768x66
        RenderText {#text} at (0,0) size 750x44
          text run at (0,0) width 750: "THIS PARAGRAPH IS UPPERCASED AND SMALL CHARACTERS IN THE SOURCE (E.G. A AND \x{C5})"
          text run at (0,22) width 378: "SHOULD THEREFORE APPEAR IN UPPERCASE. "
          text run at (378,22) width 298: "IN THE LAST SENTENCE, HOWEVER, "
        RenderInline {SPAN} at (0,0) size 761x44
          RenderText {#text} at (676,22) size 761x44
            text run at (676,22) width 85: "the last eight"
            text run at (0,44) width 210: "words should not be uppercase"
        RenderText {#text} at (210,44) size 4x22
          text run at (210,44) width 4: "."
      RenderBlock {P} at (0,438) size 768x44
        RenderText {#text} at (0,0) size 713x44
          text run at (0,0) width 713: "this paragraph is lowercased and capital characters in the source (e.g. a and \x{E5}) should therefore appear in"
          text run at (0,22) width 70: "lowercase."
      RenderTable {TABLE} at (0,498) size 768x368 [border: (1px outset #808080)]
        RenderTableSection {TBODY} at (1,1) size 766x366
          RenderTableRow {TR} at (0,0) size 766x30
            RenderTableCell {TD} at (0,0) size 766x30 [bgcolor=#C0C0C0] [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=2]
              RenderInline {STRONG} at (0,0) size 163x22
                RenderText {#text} at (4,4) size 163x22
                  text run at (4,4) width 163: "TABLE Testing Section"
          RenderTableRow {TR} at (0,30) size 766x336
            RenderTableCell {TD} at (0,183) size 12x30 [bgcolor=#C0C0C0] [border: (1px inset #808080)] [r=1 c=0 rs=1 cs=1]
              RenderText {#text} at (4,4) size 4x22
                text run at (4,4) width 4: " "
            RenderTableCell {TD} at (12,30) size 754x336 [border: (1px inset #808080)] [r=1 c=1 rs=1 cs=1]
              RenderBlock {P} at (4,4) size 746x44
                RenderText {#text} at (0,0) size 734x44
                  text run at (0,0) width 734: "This page tests the 'text-transform' property of CSS1. This paragraph has no text transformation and should"
                  text run at (0,22) width 105: "appear normal."
              RenderBlock {P} at (4,64) size 746x66
                RenderText {#text} at (0,0) size 733x66
                  text run at (0,0) width 733: "This Paragraph Is Capitalized And The First Letter In Each Word Should Therefore Appear In Uppercase."
                  text run at (0,22) width 714: "Words That Are In Uppercase In The Source (E.G. USA) Should Remain So. There Should Be A Capital"
                  text run at (0,44) width 714: "Letter After A Non-Breaking Space (&Nbsp;). Both Those Characters Appear In The Previous Sentence."
              RenderBlock {P} at (4,146) size 746x44
                RenderText {#text} at (0,0) size 722x44
                  text run at (0,0) width 722: "Words with inline elements inside them should only capitalize the first letter of the word. Therefore, the last"
                  text run at (0,22) width 407: "word in this sentence should have one, and only one, capital "
                RenderInline {SPAN} at (0,0) size 42x22
                  RenderText {#text} at (407,22) size 18x22
                    text run at (407,22) width 18: "Le"
                  RenderInline {SPAN} at (0,0) size 10x22
                    RenderText {#text} at (425,22) size 10x22
                      text run at (425,22) width 10: "tt"
                  RenderText {#text} at (435,22) size 14x22
                    text run at (435,22) width 14: "er"
                RenderText {#text} at (449,22) size 4x22
                  text run at (449,22) width 4: "."
              RenderBlock {P} at (4,206) size 746x66
                RenderText {#text} at (0,0) size 729x44
                  text run at (0,0) width 729: "THIS PARAGRAPH IS UPPERCASED AND SMALL CHARACTERS IN THE SOURCE (E.G. A AND"
                  text run at (0,22) width 399: "\x{C5}) SHOULD THEREFORE APPEAR IN UPPERCASE. "
                  text run at (399,22) width 298: "IN THE LAST SENTENCE, HOWEVER, "
                RenderInline {SPAN} at (0,0) size 745x44
                  RenderText {#text} at (697,22) size 745x44
                    text run at (697,22) width 48: "the last"
                    text run at (0,44) width 247: "eight words should not be uppercase"
                RenderText {#text} at (247,44) size 4x22
                  text run at (247,44) width 4: "."
              RenderBlock {P} at (4,288) size 746x44
                RenderText {#text} at (0,0) size 713x44
                  text run at (0,0) width 713: "this paragraph is lowercased and capital characters in the source (e.g. a and \x{E5}) should therefore appear in"
                  text run at (0,22) width 70: "lowercase."
