layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x98
  RenderBlock {HTML} at (0,0) size 800x98
    RenderBody {BODY} at (8,16) size 784x66
      RenderBlock {P} at (0,0) size 784x66
        RenderText {#text} at (0,0) size 757x66
          text run at (0,0) width 757: "Below, on the right edge of the page, there should be a green square. In the middle of such green square, a black"
          text run at (0,22) width 745: "horizontal stripe should be traversing it and protruding out of it toward the left. There should be no red in this"
          text run at (0,44) width 36: "page."
layer at (664,98) size 128x128
  RenderBlock (positioned) {DIV} at (664,98) size 128x128 [bgcolor=#FF0000]
    RenderBlock {SPAN} at (0,0) size 128x128 [bgcolor=#008000]
      RenderText {#text} at (-128,47) size 256x33
        text run at (-128,47) width 256: "XXXXXXXX"
