EDITING DELEGATE: shouldBeginEditingInDOMRange:range from 0 of DIV > BODY > HTML > #document to 1 of DIV > BODY > HTML > #document
EDITING DELEGATE: webViewDidBeginEditing:WebViewDidBeginEditingNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: shouldChangeSelectedDOMRange:(null) toDOMRange:range from 0 of LI > UL > DIV > BODY > HTML > #document to 0 of LI > UL > DIV > BODY > HTML > #document affinity:NSSelectionAffinityDownstream stillSelecting:FALSE
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock {P} at (0,0) size 784x44
        RenderText {#text} at (0,0) size 560x22
          text run at (0,0) width 560: "This test pushes a horizontal rule into an unordered list with InsertUnorderedList. "
        RenderInline {B} at (0,0) size 765x44
          RenderText {#text} at (560,0) size 765x44
            text run at (560,0) width 205: "This demonstrates what might"
            text run at (0,22) width 63: "be a bug:"
        RenderText {#text} at (63,22) size 463x22
          text run at (63,22) width 463: " the horizontal rule appears before the list marker in the render tree."
      RenderBlock {DIV} at (0,60) size 784x32
        RenderBlock {UL} at (0,0) size 784x32
          RenderListItem {LI} at (40,0) size 744x32
            RenderBlock (anonymous) at (0,0) size 744x0
            RenderBlock {HR} at (0,0) size 744x2 [border: (1px inset #000000)]
            RenderBlock (anonymous) at (0,10) size 744x22
              RenderListMarker at (-18,0) size 7x22: bullet
        RenderBlock (anonymous) at (0,48) size 784x0
caret: position 0 of child 0 {HR} of child 0 {LI} of child 0 {UL} of child 2 {DIV} of body
