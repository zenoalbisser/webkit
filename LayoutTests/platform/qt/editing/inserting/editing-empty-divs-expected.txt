EDITING DELEGATE: shouldBeginEditingInDOMRange:range from 0 of DIV > BODY > HTML > #document to 1 of DIV > BODY > HTML > #document
EDITING DELEGATE: webViewDidBeginEditing:WebViewDidBeginEditingNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: shouldChangeSelectedDOMRange:range from 0 of #text > DIV > BODY > HTML > #document to 1 of #text > DIV > BODY > HTML > #document toDOMRange:range from 1 of #text > DIV > BODY > HTML > #document to 1 of #text > DIV > BODY > HTML > #document affinity:NSSelectionAffinityDownstream stillSelecting:FALSE
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
EDITING DELEGATE: shouldEndEditingInDOMRange:range from 0 of DIV > BODY > HTML > #document to 1 of DIV > BODY > HTML > #document
EDITING DELEGATE: webViewDidEndEditing:WebViewDidEndEditingNotification
EDITING DELEGATE: shouldBeginEditingInDOMRange:range from 0 of DIV > BODY > HTML > #document to 0 of DIV > BODY > HTML > #document
EDITING DELEGATE: webViewDidBeginEditing:WebViewDidBeginEditingNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: shouldChangeSelectedDOMRange:range from 0 of DIV > BODY > HTML > #document to 0 of DIV > BODY > HTML > #document toDOMRange:range from 1 of #text > DIV > BODY > HTML > #document to 1 of #text > DIV > BODY > HTML > #document affinity:NSSelectionAffinityDownstream stillSelecting:FALSE
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
EDITING DELEGATE: shouldEndEditingInDOMRange:range from 0 of DIV > BODY > HTML > #document to 1 of DIV > BODY > HTML > #document
EDITING DELEGATE: webViewDidEndEditing:WebViewDidEndEditingNotification
EDITING DELEGATE: shouldBeginEditingInDOMRange:range from 0 of DIV > BODY > HTML > #document to 1 of DIV > BODY > HTML > #document
EDITING DELEGATE: webViewDidBeginEditing:WebViewDidBeginEditingNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: shouldChangeSelectedDOMRange:range from 0 of DIV > BODY > HTML > #document to 0 of DIV > BODY > HTML > #document toDOMRange:range from 1 of #text > DIV > BODY > HTML > #document to 1 of #text > DIV > BODY > HTML > #document affinity:NSSelectionAffinityDownstream stillSelecting:FALSE
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
EDITING DELEGATE: shouldEndEditingInDOMRange:range from 0 of DIV > BODY > HTML > #document to 2 of DIV > BODY > HTML > #document
EDITING DELEGATE: webViewDidEndEditing:WebViewDidEndEditingNotification
EDITING DELEGATE: shouldBeginEditingInDOMRange:range from 0 of DIV > BODY > HTML > #document to 1 of DIV > BODY > HTML > #document
EDITING DELEGATE: webViewDidBeginEditing:WebViewDidBeginEditingNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: shouldChangeSelectedDOMRange:range from 0 of DIV > BODY > HTML > #document to 0 of DIV > BODY > HTML > #document toDOMRange:range from 1 of #text > DIV > BODY > HTML > #document to 1 of #text > DIV > BODY > HTML > #document affinity:NSSelectionAffinityDownstream stillSelecting:FALSE
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
EDITING DELEGATE: shouldEndEditingInDOMRange:range from 0 of DIV > BODY > HTML > #document to 2 of DIV > BODY > HTML > #document
EDITING DELEGATE: webViewDidEndEditing:WebViewDidEndEditingNotification
EDITING DELEGATE: shouldBeginEditingInDOMRange:range from 0 of DIV > BODY > HTML > #document to 2 of DIV > BODY > HTML > #document
EDITING DELEGATE: webViewDidBeginEditing:WebViewDidBeginEditingNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: shouldChangeSelectedDOMRange:range from 0 of DIV > BODY > HTML > #document to 0 of DIV > BODY > HTML > #document toDOMRange:range from 1 of #text > DIV > BODY > HTML > #document to 1 of #text > DIV > BODY > HTML > #document affinity:NSSelectionAffinityDownstream stillSelecting:FALSE
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x44
        RenderText {#text} at (0,0) size 757x44
          text run at (0,0) width 605: "This tests the ability to start editing in blocks that are visible, but have little or no content. "
          text run at (605,0) width 152: "Each of the divs below"
          text run at (0,22) width 215: "should be able to accept a caret."
      RenderBlock {P} at (0,60) size 784x22
        RenderText {#text} at (0,0) size 186x22
          text run at (0,0) width 186: "This div contains some text."
      RenderBlock {DIV} at (0,98) size 707x24 [border: (1px dotted #0000FF)]
        RenderText {#text} at (1,1) size 39x22
          text run at (1,1) width 39: "chello"
      RenderBlock {P} at (0,138) size 784x22
        RenderText {#text} at (0,0) size 195x22
          text run at (0,0) width 195: "This div is completely empty."
      RenderBlock {DIV} at (0,176) size 707x24 [border: (1px dotted #0000FF)]
        RenderText {#text} at (1,1) size 7x22
          text run at (1,1) width 7: "c"
      RenderBlock {P} at (0,216) size 784x22
        RenderText {#text} at (0,0) size 360x22
          text run at (0,0) width 360: "This div contains some collapsable whitespace (a '\\n')."
      RenderBlock {DIV} at (0,254) size 707x24 [border: (1px dotted #0000FF)]
        RenderText {#text} at (1,1) size 7x22
          text run at (1,1) width 7: "c"
        RenderText {#text} at (0,0) size 0x0
      RenderBlock {P} at (0,294) size 784x22
        RenderText {#text} at (0,0) size 246x22
          text run at (0,0) width 246: "This div contains a self-closing p tag."
      RenderBlock {DIV} at (0,332) size 707x40 [border: (1px dotted #0000FF)]
        RenderBlock (anonymous) at (1,1) size 705x22
          RenderText {#text} at (0,0) size 7x22
            text run at (0,0) width 7: "c"
        RenderBlock {P} at (1,39) size 705x0
      RenderBlock {P} at (0,388) size 784x22
        RenderText {#text} at (0,0) size 762x22
          text run at (0,0) width 762: "This div contains a self-closing p tag and some collapsable whitespace (two '\\n's before and after the self closing p."
      RenderBlock {DIV} at (0,426) size 707x40 [border: (1px dotted #0000FF)]
        RenderBlock (anonymous) at (1,1) size 705x22
          RenderText {#text} at (0,0) size 7x22
            text run at (0,0) width 7: "c"
          RenderText {#text} at (0,0) size 0x0
        RenderBlock {P} at (1,39) size 705x0
caret: position 1 of child 0 {#text} of child 21 {DIV} of body
