EDITING DELEGATE: shouldBeginEditingInDOMRange:range from 0 of BODY > HTML > #document to 6 of BODY > HTML > #document
EDITING DELEGATE: webViewDidBeginEditing:WebViewDidBeginEditingNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: shouldChangeSelectedDOMRange:(null) toDOMRange:range from 0 of #text > DIV > DIV > DIV > DIV > BODY > HTML > #document to 1 of #text > DIV > DIV > DIV > DIV > BODY > HTML > #document affinity:NSSelectionAffinityDownstream stillSelecting:FALSE
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {DIV} at (0,0) size 784x94 [border: (2px solid #0000FF)]
        RenderText {#text} at (14,14) size 720x66
          text run at (14,14) width 714: "Should see the \"bar\" and \"baz\" lines centered in their own red boxes."
          text run at (728,14) width 6: " "
          text run at (14,47) width 391: "The \"foo\" line should not be centered."
      RenderBlock {DIV} at (0,118) size 784x155
        RenderBlock {DIV} at (0,0) size 784x155 [border: (2px solid #FF0000)]
          RenderBlock (anonymous) at (14,14) size 756x33
            RenderText {#text} at (0,0) size 32x33
              text run at (0,0) width 32: "foo"
          RenderBlock {DIV} at (14,47) size 756x94 [border: (2px solid #FF0000)]
            RenderBlock {DIV} at (14,14) size 728x33
              RenderText {#text} at (346,0) size 42x33
                text run at (346,0) width 36: "bar"
                text run at (382,0) width 6: " "
            RenderBlock {DIV} at (14,47) size 728x33
              RenderText {#text} at (346,0) size 36x33
                text run at (346,0) width 36: "baz"
            RenderBlock (anonymous) at (14,80) size 728x0
selection start: position 0 of child 0 {#text} of child 0 {DIV} of child 1 {DIV} of child 1 {DIV} of child 3 {DIV} of body
selection end:   position 1 of child 0 {#text} of child 1 {DIV} of child 1 {DIV} of child 1 {DIV} of child 3 {DIV} of body
