layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x44
        RenderText {#text} at (0,0) size 111x22
          text run at (0,0) width 111: "This is a test for "
        RenderInline {I} at (0,0) size 721x44
          RenderInline {A} at (0,0) size 310x22 [color=#0000EE]
            RenderText {#text} at (111,0) size 310x22
              text run at (111,0) width 310: "https://bugs.webkit.org/show_bug.cgi?id=18809"
          RenderText {#text} at (421,0) size 721x44
            text run at (421,0) width 4: " "
            text run at (425,0) width 296: "Forms with block level generated content and"
            text run at (0,22) width 414: "absolutely positioned labels break inline layout (fixed on reflow)"
        RenderText {#text} at (414,22) size 4x22
          text run at (414,22) width 4: "."
      RenderBlock {DIV} at (0,60) size 784x44
        RenderBlock (anonymous) at (0,0) size 784x22
          RenderText {#text} at (0,0) size 103x22
            text run at (0,0) width 103: "This should be "
          RenderText {#text} at (103,0) size 82x22
            text run at (103,0) width 82: "a single line."
        RenderBlock (generated) at (0,22) size 784x22
          RenderText at (0,0) size 206x22
            text run at (0,0) width 206: "This should be the second line."
      RenderBlock {DIV} at (0,104) size 784x22
        RenderBlock {DIV} at (0,0) size 784x0
        RenderBlock (anonymous) at (0,0) size 784x22
          RenderText {#text} at (0,0) size 103x22
            text run at (0,0) width 103: "This should be "
          RenderText {#text} at (103,0) size 82x22
            text run at (103,0) width 82: "a single line."
      RenderBlock {DIV} at (0,126) size 784x22
        RenderBlock {DIV} at (0,0) size 784x0
        RenderBlock (anonymous) at (0,0) size 784x22
          RenderText {#text} at (103,0) size 82x22
            text run at (103,0) width 82: "a single line."
          RenderBlock (floating) {SPAN} at (0,0) size 103x22
            RenderText {#text} at (0,0) size 103x22
              text run at (0,0) width 103: "This should be "
