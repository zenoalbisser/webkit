layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x280
  RenderBlock {HTML} at (0,0) size 800x280
    RenderBody {BODY} at (8,16) size 784x256
      RenderBlock {P} at (0,0) size 784x22
        RenderInline {B} at (0,0) size 62x22
          RenderText {#text} at (0,0) size 62x22
            text run at (0,0) width 62: "BUG ID:"
        RenderText {#text} at (62,0) size 4x22
          text run at (62,0) width 4: " "
        RenderInline {A} at (0,0) size 121x22 [color=#0000EE]
          RenderText {#text} at (66,0) size 121x22
            text run at (66,0) width 121: "Bugzilla bug 6770"
        RenderText {#text} at (187,0) size 466x22
          text run at (187,0) width 466: " REGRESSION: Incomplete repaint when block with clipping grows"
      RenderBlock {P} at (0,38) size 784x50 [bgcolor=#98FB98]
        RenderInline {B} at (0,0) size 91x22
          RenderText {#text} at (3,3) size 91x22
            text run at (3,3) width 91: "TEST PASS:"
        RenderText {#text} at (94,3) size 775x44
          text run at (94,3) width 4: " "
          text run at (98,3) width 680: "There should be a white square with a blue border below, and no line running through the middle of"
          text run at (3,25) width 75: "the square."
      RenderBlock {P} at (0,104) size 784x28 [bgcolor=#FF3300]
        RenderInline {B} at (0,0) size 90x22
          RenderText {#text} at (3,3) size 90x22
            text run at (3,3) width 90: "TEST FAIL:"
        RenderText {#text} at (93,3) size 649x22
          text run at (93,3) width 4: " "
          text run at (97,3) width 645: "The white square will have a blue border and a blue horizontal line running through its middle."
layer at (8,164) size 784x108
  RenderBlock {DIV} at (0,148) size 784x108
    RenderBlock {DIV} at (0,0) size 108x108 [border: (4px solid #0000FF)]
      RenderBlock {DIV} at (124,4) size 10x100
