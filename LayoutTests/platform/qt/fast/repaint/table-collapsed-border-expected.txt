layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,10) size 784x580
      RenderTable {TABLE} at (0,0) size 101x88 [border: (4px solid #008000)]
        RenderTableSection {TBODY} at (4,4) size 93x80
          RenderTableRow {TR} at (0,0) size 93x28
            RenderTableCell {TD} at (0,11) size 6x6 [border: (4px none #000000)] [r=0 c=0 rs=1 cs=1]
            RenderTableCell {TD} at (6,0) size 87x28 [border: (4px none #000000)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (1,5) size 46x22
                text run at (1,5) width 46: "Lorem"
          RenderTableRow {TR} at (0,28) size 93x24
            RenderTableCell {TD} at (0,39) size 6x2 [border: none] [r=1 c=0 rs=1 cs=1]
            RenderTableCell {TD} at (6,28) size 87x24 [border: none] [r=1 c=1 rs=1 cs=1]
              RenderText {#text} at (1,1) size 81x22
                text run at (1,1) width 81: "ipsum dolor"
          RenderTableRow {TR} at (0,52) size 93x28
            RenderTableCell {TD} at (0,63) size 6x6 [border: none] [r=2 c=0 rs=1 cs=1]
            RenderTableCell {TD} at (6,52) size 87x28 [border: none] [r=2 c=1 rs=1 cs=1]
              RenderText {#text} at (1,1) size 52x22
                text run at (1,1) width 52: "sit amet"
      RenderTable {TABLE} at (0,98) size 76x88 [border: (4px solid #000000)]
        RenderTableSection {TBODY} at (4,4) size 68x80
          RenderTableRow {TR} at (0,0) size 68x28
            RenderTableCell {TD} at (0,11) size 10x6 [border: (4px none #000000)] [r=0 c=0 rs=1 cs=1]
            RenderTableCell {TD} at (10,0) size 58x28 [border: (4px none #000000)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (1,5) size 46x22
                text run at (1,5) width 46: "Lorem"
          RenderTableRow {TR} at (0,28) size 68x24
            RenderTableCell {TD} at (0,28) size 10x24 [border: none] [r=1 c=0 rs=1 cs=1]
              RenderText {#text} at (5,1) size 4x22
                text run at (5,1) width 4: " "
          RenderTableRow {TR} at (0,52) size 68x28
            RenderTableCell {TD} at (0,63) size 10x6 [border: none] [r=2 c=0 rs=1 cs=1]
            RenderTableCell {TD} at (10,52) size 58x28 [border: none] [r=2 c=1 rs=1 cs=1]
              RenderText {#text} at (1,1) size 52x22
                text run at (1,1) width 52: "sit amet"
      RenderTable {TABLE} at (0,196) size 115x88 [border: (4px solid #000000)]
        RenderTableSection {TBODY} at (4,4) size 107x80
          RenderTableRow {TR} at (0,0) size 107x28
            RenderTableCell {TD} at (0,11) size 10x6 [border: (4px none #000000)] [r=0 c=0 rs=1 cs=1]
            RenderTableCell {TD} at (10,0) size 97x28 [border: (4px none #000000)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (1,5) size 91x22
                text run at (1,5) width 91: "Lorem ipsum"
          RenderTableRow {TR} at (0,28) size 107x24
            RenderTableCell {TD} at (0,28) size 10x24 [border: none] [r=1 c=0 rs=1 cs=1]
              RenderText {#text} at (5,1) size 4x22
                text run at (5,1) width 4: " "
          RenderTableRow {TR} at (0,52) size 107x28
            RenderTableCell {TD} at (0,63) size 10x6 [border: none] [r=2 c=0 rs=1 cs=1]
            RenderTableCell {TD} at (10,52) size 97x28 [border: none] [r=2 c=1 rs=1 cs=1]
              RenderText {#text} at (1,1) size 52x22
                text run at (1,1) width 52: "sit amet"
      RenderTable {TABLE} at (0,294) size 105x88 [border: (4px solid #000000)]
        RenderTableSection {TBODY} at (4,4) size 97x80
          RenderTableRow {TR} at (0,0) size 97x28
            RenderTableCell {TD} at (0,11) size 10x6 [border: (4px none #000000)] [r=0 c=0 rs=1 cs=1]
            RenderTableCell {TD} at (10,0) size 87x28 [border: (4px none #000000)] [r=0 c=1 rs=1 cs=1]
              RenderText {#text} at (1,5) size 46x22
                text run at (1,5) width 46: "Lorem"
          RenderTableRow {TR} at (0,28) size 97x24
            RenderTableCell {TD} at (0,28) size 10x24 [border: none] [r=1 c=0 rs=1 cs=1]
              RenderText {#text} at (5,1) size 4x22
                text run at (5,1) width 4: " "
            RenderTableCell {TD} at (10,28) size 87x24 [border: none] [r=1 c=1 rs=1 cs=1]
              RenderText {#text} at (1,1) size 81x22
                text run at (1,1) width 81: "ipsum dolor"
          RenderTableRow {TR} at (0,52) size 97x28
            RenderTableCell {TD} at (0,63) size 10x6 [border: none] [r=2 c=0 rs=1 cs=1]
            RenderTableCell {TD} at (10,52) size 87x28 [border: none] [r=2 c=1 rs=1 cs=1]
              RenderText {#text} at (1,1) size 52x22
                text run at (1,1) width 52: "sit amet"
