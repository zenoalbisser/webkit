layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x206
  RenderBlock {HTML} at (0,0) size 800x206
    RenderBody {BODY} at (8,16) size 784x174
      RenderBlock {DIV} at (0,0) size 784x174
        RenderBlock {P} at (0,0) size 784x22 [bgcolor=#00FF00]
          RenderText {#text} at (0,0) size 337x22
            text run at (0,0) width 337: "This paragraph should have a green background."
        RenderBlock {P} at (0,38) size 784x22
          RenderText {#text} at (0,0) size 215x22
            text run at (0,0) width 215: "But this one should be unstyled."
        RenderBlock {P} at (0,76) size 784x22
          RenderText {#text} at (0,0) size 250x22
            text run at (0,0) width 250: "And this one should also be unstyled."
        RenderBlock {ADDRESS} at (0,114) size 784x22 [bgcolor=#00FF00]
          RenderText {#text} at (0,0) size 690x22
            text run at (0,0) width 690: "This address is only here to fill some space between two paragraphs and should have a green background."
        RenderBlock {P} at (0,152) size 784x22 [bgcolor=#00FF00]
          RenderText {#text} at (0,0) size 362x22
            text run at (0,0) width 362: "This paragraph should have a green background too."
