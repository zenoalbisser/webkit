layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock {P} at (0,0) size 784x22
        RenderText {#text} at (0,0) size 98x22
          text run at (0,0) width 98: "This tests that "
        RenderInline {TT} at (0,0) size 30x17
          RenderText {#text} at (98,3) size 30x17
            text run at (98,3) width 30: "WBR"
        RenderText {#text} at (128,0) size 324x22
          text run at (128,0) width 324: " elements cannot be styled and that setting their "
        RenderInline {TT} at (0,0) size 45x17
          RenderText {#text} at (452,3) size 45x17
            text run at (452,3) width 45: "position"
        RenderText {#text} at (497,0) size 21x22
          text run at (497,0) width 21: " to "
        RenderInline {TT} at (0,0) size 49x17
          RenderText {#text} at (518,3) size 49x17
            text run at (518,3) width 49: "absolute"
        RenderText {#text} at (567,0) size 162x22
          text run at (567,0) width 162: " does not crash WebKit."
      RenderBlock {P} at (0,38) size 784x22
        RenderText {#text} at (0,0) size 135x22
          text run at (0,0) width 135: "There should be no "
        RenderWordBreak {WBR} at (0,0) size 0x0
        RenderText {#text} at (135,0) size 104x22
          text run at (135,0) width 104: "red on this line."
      RenderBlock {P} at (0,76) size 784x22
        RenderText {#text} at (0,0) size 131x22
          text run at (0,0) width 131: "No crashing, please"
        RenderWordBreak {WBR} at (0,0) size 0x0
        RenderText {#text} at (131,0) size 4x22
          text run at (131,0) width 4: "."
