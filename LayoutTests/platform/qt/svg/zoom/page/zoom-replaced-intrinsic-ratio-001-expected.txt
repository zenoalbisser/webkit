layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x385
  RenderBlock {HTML} at (0,0) size 800x385
    RenderBody {BODY} at (3,3) size 117x376 [border: (1px dashed #C0C0C0)]
      RenderBlock {P} at (1,8) size 115x21
        RenderText {#text} at (0,-2) size 115x25
          text run at (0,-2) width 113: "The following six blue boxes must"
          text run at (0,5) width 115: "be of the same width. There must"
          text run at (0,12) width 34: "be no red."
      RenderBlock {P} at (8,36) size 101x21 [bgcolor=#008000] [border: (7px solid #0000FF)]
        RenderText {#text} at (7,5) size 2x11
          text run at (7,5) width 2: " "
      RenderBlock {P} at (1,103) size 115x7
        RenderText {#text} at (0,-2) size 12x11
          text run at (0,-2) width 12: "      "
        RenderText {#text} at (0,0) size 0x0
        RenderEmbeddedObject {OBJECT} at (7,0) size 101x36 [bgcolor=#FF0000] [border: (7px solid #0000FF)]
          layer at (0,0) size 87x22
            RenderView at (0,0) size 87x22
          layer at (0,0) size 87x22
            RenderSVGRoot {svg} at (0,0) size 87x22
              RenderSVGPath {rect} at (0,0) size 87x22 [stroke={[type=SOLID] [color=#008000] [stroke width=12.00]}] [fill={[type=SOLID] [color=#00FF00]}] [x=0.00] [y=0.00] [width=1000.00] [height=250.00]
              RenderSVGPath {path} at (13,4) size 61x14 [fill={[type=SOLID] [color=#008000]}] [data="M 500 50 L 150 200 L 850 200 Z"]
      RenderBlock {P} at (1,156) size 115x7
        RenderEmbeddedObject {OBJECT} at (7,0) size 101x36 [bgcolor=#FF0000] [border: (7px solid #0000FF)]
          layer at (0,0) size 87x22
            RenderView at (0,0) size 87x22
          layer at (0,0) size 87x22
            RenderSVGRoot {svg} at (0,0) size 87x22
              RenderSVGPath {rect} at (0,0) size 87x22 [stroke={[type=SOLID] [color=#008000] [stroke width=12.00]}] [fill={[type=SOLID] [color=#00FF00]}] [x=0.00] [y=0.00] [width=1000.00] [height=250.00]
              RenderSVGPath {path} at (13,4) size 61x14 [fill={[type=SOLID] [color=#008000]}] [data="M 500 50 L 150 200 L 850 200 Z"]
      RenderTable at (1,209) size 115x37
        RenderTableSection (anonymous) at (0,0) size 115x37
          RenderTableRow (anonymous) at (0,0) size 115x37
            RenderTableCell {P} at (0,0) size 115x37 [r=0 c=0 rs=1 cs=1]
              RenderEmbeddedObject {OBJECT} at (7,0) size 101x36 [bgcolor=#FF0000] [border: (7px solid #0000FF)]
                layer at (0,0) size 87x22
                  RenderView at (0,0) size 87x22
                layer at (0,0) size 87x22
                  RenderSVGRoot {svg} at (0,0) size 87x22
                    RenderSVGPath {rect} at (0,0) size 87x22 [stroke={[type=SOLID] [color=#008000] [stroke width=12.00]}] [fill={[type=SOLID] [color=#00FF00]}] [x=0.00] [y=0.00] [width=1000.00] [height=250.00]
                    RenderSVGPath {path} at (13,4) size 61x14 [fill={[type=SOLID] [color=#008000]}] [data="M 500 50 L 150 200 L 850 200 Z"]
      RenderTable {TABLE} at (1,292) size 115x37
        RenderTableSection {TBODY} at (0,0) size 115x37
          RenderTableRow {TR} at (0,0) size 115x37
            RenderTableCell {TD} at (0,0) size 115x37 [r=0 c=0 rs=1 cs=1]
              RenderEmbeddedObject {OBJECT} at (7,0) size 101x36 [bgcolor=#FF0000] [border: (7px solid #0000FF)]
                layer at (0,0) size 87x22
                  RenderView at (0,0) size 87x22
                layer at (0,0) size 87x22
                  RenderSVGRoot {svg} at (0,0) size 87x22
                    RenderSVGPath {rect} at (0,0) size 87x22 [stroke={[type=SOLID] [color=#008000] [stroke width=12.00]}] [fill={[type=SOLID] [color=#00FF00]}] [x=0.00] [y=0.00] [width=1000.00] [height=250.00]
                    RenderSVGPath {path} at (13,4) size 61x14 [fill={[type=SOLID] [color=#008000]}] [data="M 500 50 L 150 200 L 850 200 Z"]
      RenderBlock (floating) {P} at (1,375) size 115x7
        RenderEmbeddedObject {OBJECT} at (7,0) size 101x36 [bgcolor=#FF0000] [border: (7px solid #0000FF)]
          layer at (0,0) size 87x22
            RenderView at (0,0) size 87x22
          layer at (0,0) size 87x22
            RenderSVGRoot {svg} at (0,0) size 87x22
              RenderSVGPath {rect} at (0,0) size 87x22 [stroke={[type=SOLID] [color=#008000] [stroke width=12.00]}] [fill={[type=SOLID] [color=#00FF00]}] [x=0.00] [y=0.00] [width=1000.00] [height=250.00]
              RenderSVGPath {path} at (13,4) size 61x14 [fill={[type=SOLID] [color=#008000]}] [data="M 500 50 L 150 200 L 850 200 Z"]
